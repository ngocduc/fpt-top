const gameSchema = require('./generic/game+v1.json');
const userSchema = require('./generic/user+v1.json');

const deleteRequestSchema = require('./request/delete/favorite+request+delete+one+game+v1.1.json');
const postPutRequestSchema = require('./request/post-put/favorite+request+post+put+v1.json');
const patchRequestSchema = require('./request/patch/favorite+request+patch+v1.json');

module.exports = {
  gameSchema,
  userSchema,
  deleteRequestSchema,
  postPutRequestSchema,
  patchRequestSchema,
};
