require('dotenv').config();

const PORT = process.env.PORT || 3000;
const { USER_NAME, PASS, ENDPOINT } = process.env;
const DB = `mongodb+srv://${USER_NAME}:${PASS}@${ENDPOINT}`;

module.exports = {
  DB,
  PORT,
};
