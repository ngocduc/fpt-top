const { merge } = require('lodash');
const Favorite = require('../model/favoriteModel');
const {
  ajvGame,
  ajvDel,
} = require('../validate');

const checkValidGameReq = data => ajvGame.validate(data);
const checkValidDelReq = data => ajvDel.validate(data);

const getAll = async (req, res) => {
  try {
    const data = await Favorite.find();
    if (data.length) res.status(200).send(data);
    else res.status(404).send('No data');
  } catch (e) {
    throw e;
  }
};

const removeAll = async (req, res) => {
  try {
    const data = await Favorite.remove({});
    res.send(data);
  } catch (e) {
    throw e;
  }
};

const createUserInfo = async (req, res) => {
  const { body } = req;
  try {
    const data = new Favorite(body);
    const result = await data.save();
    res.send(result);
  } catch (e) {
    throw e;
  }
};

const getListGames = async (req, res) => {
  const { userId } = req.params;
  try {
    const userData = await Favorite.findOne({ userId });
    if (userData) res.status(200).send(userData.games);
    else res.status(404).send('User not found');
  } catch (e) {
    res.status(500).json(e);
  }
};

const getGameDetail = async (req, res) => {
  const { userId, gameName } = req.params;
  try {
    const userData = await Favorite.findOne({ userId });
    if (userData) res.send(userData.games.find(v => (v.name || '@').toLowerCase() === gameName.toLowerCase()));
    else res.status(404).send('User not found');
  } catch (e) {
    res.status(500).json(e);
  }
};

const addGame = async (req, res) => {
  const { data, userId } = req.body;
  if (Array.isArray(data)) { // If data to save is an array, we replace old list
    try {
      data.forEach(v => checkValidGameReq(v));
      const result = await Favorite.updateOne(
        { userId },
        { games: data },
      );
      if (result.n) res.status(201).send('Added list game success');
      else res.status(404).send('Error: userId can not found');
    } catch (error) {
      res.status(422).send('The data has invalid schema');
    }
  } else { // If data is object we only push it to the old list
    try {
      checkValidGameReq(data);
      const gameName = data.name;
      const gameData = Favorite.findOne({ userId, gameName });
      if (gameData) {
        res.status(400).send('Game already in list');
      } else {
        const result = await Favorite.updateOne(
          { userId },
          { $push: { games: data } },
        );
        if (result.n) res.status(201).send('Added game success');
        else res.status(404).send('Error: userId can not found');
      }
    } catch (e) {
      res.status(422).send('The data has invalid schema');
    }
  }
};

const updateGame = async (req, res) => {
  const { gameName, userId, data } = req.body;
  try {
    checkValidGameReq(data);
    const userData = await Favorite.findOne({ userId });
    if (userData) {
      const gameData = userData.games.find(v => v.name.toLowerCase() === gameName.toLowerCase());
      const update = userData.games.filter(v => v.name.toLowerCase() !== gameName.toLowerCase());
      if (gameData) {
        update.push(merge(gameData, data));
        await Favorite.updateOne(
          { userId },
          { games: update },
        );
        res.status(200).send('Updated success!');
      } else {
        res.status(400).send('Game does not exist');
      }
    } else {
      res.status(400).send('User does not exist');
    }
  } catch (error) {
    res.status(422).send('The data has invalid schema');
  }
};

const deleleGame = async (req, res) => {
  const { body } = req;
  try {
    checkValidDelReq(body);
    const { gameName, userId } = body;
    const userData = await Favorite.findOne({ userId });
    if (userData && gameName) {
      const gameData = userData.games.find(v => v.name.toLowerCase() === gameName.toLowerCase());
      if (gameData) {
        const update = userData.games.filter(v => v.name.toLowerCase() !== gameName.toLowerCase());
        await Favorite.updateOne(
          { userId },
          { games: update },
        );
        res.status(204).end();
      } else {
        res.status(400).send('Game does not exist');
      }
    } else if (userData && !gameName) {
      await Favorite.updateOne(
        { userId },
        { games: [] },
      );
      res.status(204).end();
    } else {
      res.status(400).send('User does not exist');
    }
  } catch (error) {
    res.status(422).send('The data has invalid schema');
  }
};

module.exports = {
  getListGames,
  getGameDetail,
  addGame,
  updateGame,
  deleleGame,
  getAll,
  createUserInfo,
  removeAll,
};
