const mongoose = require('mongoose');

const { Schema } = mongoose;

const requireField = function requireField() {
  return this.type !== 'other' && this.type !== '';
};
const requireExistArray = function requireExistArray(v) {
  return requireField(v) && v.length > 0 && v[0].length > 0;
};

const gameSchema = {
  name: {
    type: String,
    trim: true,
    require: true,
  },
  is_free: {
    type: Boolean,
    require: true,
  },
  description: String,
  developers: {
    type: Array,
    required: true,
    validate: [requireExistArray, 'path is required and with atleast one string'],
  },
  publishers: [{
    type: String,
    require: true,
  }],
  platforms: {
    windows: {
      type: Boolean,
      require: true,
    },
    mac: {
      type: Boolean,
      require: true,
    },
    linux: {
      type: Boolean,
      require: true,
    },
  },
  genres: {
    type: String,
    enum: ['Action', 'Adventure', 'Casual', 'Indie', 'Rpg', 'Other'],
    required: [true, "can't be empty"],
  },
  release_date: {
    type: Date,
    require: true,
  },
  price: {
    amount: {
      type: Number,
      min: 0,
      require: true,
    },
    currency: {
      type: String,
      enum: ['VND', 'USD'],
      require: true,
    },
  },
};

const favoriteSchema = new Schema({
  userId: {
    type: String,
    minlength: 6,
  },
  games: [gameSchema],
});

// Middleware and hook will below here

module.exports = mongoose.model('Favorite', favoriteSchema);
