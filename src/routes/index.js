const express = require('express');
const {
  getListGames,
  getGameDetail,
  addGame,
  updateGame,
  deleleGame,
  getAll,
  removeAll,
  createUserInfo,
} = require('../controller');

const app = express();
const router = express.Router();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Get list game of user
router.get('/favorites/:userId', getListGames);

// Get specific game of user by user id
router.get('/favorites/:userId/:gameName', getGameDetail);

// Add a game to list
// Or
// Add a list game (replace old list if it is existed)
router.post('/favorites/', addGame);

// Update a game in list
router.put('/favorites/', updateGame);

// Delete user's list game
// Or
// Delete specific game in the list
router.delete('/favorites/', deleleGame);

// Get all
router.get('/all', getAll);

// Delete all
router.delete('/all', removeAll);

// Post one
router.post('/all', createUserInfo);

// 404 handle
router.get('/*', (req, res) => res.send('not found'));

module.exports = router;
