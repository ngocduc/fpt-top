/* eslint-disable import/no-extraneous-dependencies */
const express = require('express');
const mongoose = require('mongoose');
const signale = require('signale');
const bodyParser = require('body-parser');
const router = require('./routes');
const config = require('./config');

const app = express();

app.use(bodyParser.urlencoded({ extended: 'true' }));
app.use(bodyParser.json());
app.use('/', router);

mongoose.connect(config.DB, { useNewUrlParser: true }, (err) => {
  if (err) throw err;
});

mongoose.connection.on('connected', () => {
  signale.success(`Success connect to ${config.DB}`);
});
mongoose.connection.on('error', (err) => {
  signale.fatal(`Can not connect: ${err}`);
});

app.listen(config.PORT, (err) => {
  if (err) throw err;
  signale.debug(`App listening on port ${config.PORT}`);
});

module.exports = app;
