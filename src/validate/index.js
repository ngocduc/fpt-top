const Ajv = require('ajv');
const {
  postPutRequestSchema,
  deleteRequestSchema,
} = require('../../resources/schema/index');

const ajvGame = new Ajv({
  schemas: [
    postPutRequestSchema,
  ],
  allErrors: true,
});

const ajvDel = new Ajv({
  schemas: [
    deleteRequestSchema,
  ],
  allErrors: true,
});

module.exports = {
  ajvGame,
  ajvDel,
};
