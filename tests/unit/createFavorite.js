const sinon = require('sinon');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const { expect } = chai;

const Favorite = require('../../src/model/favoriteModel');
const app = require('../../src/server');

const gameInfo = require('./data/game+info+counter+strike.json');

const userId = 'sampleId';

describe('addGame function', () => {
  it('User can push a game with valid schema to existing list', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns(null);
    const createGameMock = sinon.stub(Favorite, 'updateOne');
    const expectedResult = { n: 1 }; // should be more keys but it can be simple like this
    createGameMock.returns(expectedResult);
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: gameInfo,
      })
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.text).to.deep.equal('Added game success');
        createGameMock.restore();
        findGameMock.restore();
        done();
      });
  });

  it('should return an error if the game info to add is not valid', (done) => {
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: 'invalid',
      })
      .end((err, res) => {
        expect(res).to.have.status(422);
        expect(res.text).to.deep.equal('The data has invalid schema');
        done();
      });
  });

  it('should return an error if the game is already in list', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns({});
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: gameInfo,
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.text).to.deep.equal('Game already in list');
        findGameMock.restore();
        done();
      });
  });

  it('should return error if user not found', (done) => {
    const createGameMock = sinon.stub(Favorite, 'updateOne');
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns(null);
    createGameMock.returns({ n: 0 });
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: gameInfo,
      })
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.text).to.deep.equal('Error: userId can not found');
        createGameMock.restore();
        findGameMock.restore();
        done();
      });
  });
});

describe('createListGame function', () => {
  it('should replace the old list if success', (done) => {
    const createListGameMock = sinon.stub(Favorite, 'updateOne');
    const expectedResult = { n: 1 }; // should be more keys but it can be simple like this
    createListGameMock.returns(expectedResult);
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: [gameInfo],
      })
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.text).to.deep.equal('Added list game success');
        createListGameMock.restore();
        done();
      });
  });

  it('should return an error message if the data to save has invalid schema', (done) => {
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: [1],
      })
      .end((err, res) => {
        expect(res).to.have.status(422);
        expect(res.text).to.deep.equal('The data has invalid schema');
        done();
      });
  });

  it('should return error if user not found', (done) => {
    const createListGameMock = sinon.stub(Favorite, 'updateOne');
    const expectedResult = { n: 0 }; // should be more keys but it can be simple like this
    createListGameMock.returns(expectedResult);
    chai.request(app)
      .post('/favorites/')
      .send({
        userId,
        data: [gameInfo],
      })
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.text).to.deep.equal('Error: userId can not found');
        createListGameMock.restore();
        done();
      });
  });
});
