const sinon = require('sinon');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const { expect } = chai;

const Favorite = require('../../src/model/favoriteModel');
const app = require('../../src/server');

const userInfo = require('./data/user+info.json');

const userId = 'sampleId';
const gameName = 'Counter-Strike';

describe('deleleGame function', () => {
  it('User can delete a game existing in list success', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    const deleleGameMock = sinon.stub(Favorite, 'updateOne');
    findGameMock.returns(userInfo);
    deleleGameMock.returns({ n: 1 });
    chai.request(app)
      .delete('/favorites/')
      .send({
        userId,
        gameName,
      })
      .end((err, res) => {
        expect(res).to.have.status(204);
        findGameMock.restore();
        deleleGameMock.restore();
        done();
      });
  });

  it("User can delete entire their list's game", (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    const deleleListGameMock = sinon.stub(Favorite, 'updateOne');
    findGameMock.returns(userInfo);
    deleleListGameMock.returns({ n: 1 });
    chai.request(app)
      .delete('/favorites/')
      .send({
        userId,
      })
      .end((err, res) => {
        expect(res).to.have.status(204);
        findGameMock.restore();
        deleleListGameMock.restore();
        done();
      });
  });

  it('should return an error if request has invalid schema', (done) => {
    chai.request(app)
      .delete('/favorites/')
      .send([])
      .end((err, res) => {
        expect(res).to.have.status(422);
        expect(res.text).to.deep.equal('The data has invalid schema');
        done();
      });
  });

  it('should return an error if user not found', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns(null);
    chai.request(app)
      .delete('/favorites/')
      .send({
        userId,
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.text).to.deep.equal('User does not exist');
        findGameMock.restore();
        done();
      });
  });

  it('should return an error if game not found', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns(userInfo);
    chai.request(app)
      .delete('/favorites/')
      .send({
        userId,
        gameName: 'not existed',
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.text).to.deep.equal('Game does not exist');
        findGameMock.restore();
        done();
      });
  });
});
