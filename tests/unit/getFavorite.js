const sinon = require('sinon');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const { expect } = chai;

const Favorite = require('../../src/model/favoriteModel');
const app = require('../../src/server');

const userInfo = require('./data/user+favorite+without+counterStrike.json');

const userId = 'sampleId';
const gameName = 'Counter-Strike: Global Offensive';

describe("Get user's list game", () => {
  it('should return all game in the list success', (done) => {
    const GetListGamesMock = sinon.stub(Favorite, 'findOne');
    GetListGamesMock.returns(userInfo);
    chai.request(app)
      .get(`/favorites/${userId}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.an('array').to.have.lengthOf(4);
        expect(res.body[0]).to.include.all.keys(
          'name',
          'is_free',
          'developers',
          'publishers',
          'platforms',
          'genres',
          'release_date',
          'price',
        );
        expect(res.body[0].name).to.deep.equal('Counter-Strike: Global Offensive');
        GetListGamesMock.restore();
        done();
      });
  });

  it('should return empty array if user does not favorite any game', (done) => {
    const GetListGamesMock = sinon.stub(Favorite, 'findOne');
    GetListGamesMock.returns({
      userId: 'someID',
      games: [],
    });
    chai.request(app)
      .get(`/favorites/${userId}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.an('array').to.have.lengthOf(0);
        GetListGamesMock.restore();
        done();
      });
  });

  it('should return error 404 if user not found', (done) => {
    const GetListGamesMock = sinon.stub(Favorite, 'findOne');
    GetListGamesMock.returns(null);
    chai.request(app)
      .get(`/favorites/${userId}`)
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.text).to.deep.equal('User not found');
        GetListGamesMock.restore();
        done();
      });
  });

  it('should return error if server has error', (done) => {
    const GetListGamesMock = sinon.stub(Favorite, 'findOne');
    const expectedResult = {
      error: 'Something went wrong',
    };
    GetListGamesMock.throws(expectedResult, null);
    chai.request(app)
      .get(`/favorites/${userId}`)
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res.body).to.deep.equal({ error: 'Something went wrong' });
        GetListGamesMock.restore();
        done();
      });
  });
});

describe("Get game detail in user's list game", () => {
  it('should return game detail in the list success', (done) => {
    const getGameDetailMock = sinon.stub(Favorite, 'findOne');
    getGameDetailMock.returns(userInfo);
    chai.request(app)
      .get(`/favorites/${userId}/${gameName}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.include.all.keys(
          'name',
          'is_free',
          'developers',
          'publishers',
          'platforms',
          'genres',
          'release_date',
          'price',
        );
        expect(res.body.name).to.deep.equal(gameName);
        getGameDetailMock.restore();
        done();
      });
  });

  it('should return an error message if user does not exist', (done) => {
    const getGameDetailMock = sinon.stub(Favorite, 'findOne');
    const expectedResult = {
      error: 'Something went wrong',
    };
    getGameDetailMock.throws(expectedResult);
    chai.request(app)
      .get(`/favorites/${userId}/${gameName}`)
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res.body).to.deep.equal({ error: 'Something went wrong' });
        getGameDetailMock.restore();
        done();
      });
  });

  it('should return error if server has error', (done) => {
    const getGameDetailMock = sinon.stub(Favorite, 'findOne');
    getGameDetailMock.returns(null);
    chai.request(app)
      .get(`/favorites/${userId}/${gameName}`)
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.text).to.deep.equal('User not found');
        getGameDetailMock.restore();
        done();
      });
  });
});
