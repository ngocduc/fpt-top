const sinon = require('sinon');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const { expect } = chai;
const app = require('../../src/server');

describe('When resource not available user will get 404 status', () => {
  it('should work', function (done) {
    chai.request(app)
      .get('/forbiden')
      .end((err, res)=>{
        expect(res.text).to.deep.equal('not found');
      })
  });
});