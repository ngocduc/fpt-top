const sinon = require('sinon');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const { expect } = chai;

const Favorite = require('../../src/model/favoriteModel');
const app = require('../../src/server');

const userInfo = require('./data/user+favorite+without+counterStrike.json');

const userId = 'sampleId';
const gameName = 'Counter-Strike: Global Offensive';

describe('updateGame function', () => {
  it('User can update a game if it is existing in list success', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    const updateGameMock = sinon.stub(Favorite, 'updateOne');
    findGameMock.returns(userInfo);
    updateGameMock.returns({ n: 1 }); // For simple demonstration
    const dataUpdate = { is_free: true };
    chai.request(app)
      .put('/favorites/')
      .send({
        userId,
        gameName,
        data: dataUpdate,
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.deep.equal('Updated success!');
        findGameMock.restore();
        updateGameMock.restore();
        done();
      });
  });

  it('should return an error if the game info to update is not valid', (done) => {
    chai.request(app)
      .put('/favorites/')
      .send({
        userId,
        gameName,
        data: 123,
      })
      .end((err, res) => {
        expect(res).to.have.status(422);
        expect(res.text).to.deep.equal('The data has invalid schema');
        done();
      });
  });

  it('should return an error if user not found', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns(null);
    const dataUpdate = { is_free: true };
    chai.request(app)
      .put('/favorites/')
      .send({
        userId,
        gameName,
        data: dataUpdate,
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.text).to.deep.equal('User does not exist');
        findGameMock.restore();
        done();
      });
  });

  it('should return an error if the game does not exist', (done) => {
    const findGameMock = sinon.stub(Favorite, 'findOne');
    findGameMock.returns({
      userId: 1,
      games: [
        {
          name: 'Diablo',
        },
      ],
    });
    const dataUpdate = { is_free: true };
    chai.request(app)
      .put('/favorites/')
      .send({
        userId,
        gameName,
        data: dataUpdate,
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.text).to.deep.equal('Game does not exist');
        findGameMock.restore();
        done();
      });
  });
});
